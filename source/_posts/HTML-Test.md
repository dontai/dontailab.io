layout: '[post]'
title: HTML-Test
date: 2017-02-21 15:19:30
tags:
---
Markdown to HTML Test Page
---

This page is for testing HTML.

**strong**

Inline link [Don's WP Blog](http://dontai.com/wp "Don's WP blog")

Lists
* apple
* pear
* orange

1. apple
2. pear
3. orange

blockquote
>'Tis the season for all to be happy. Really happy.
> happy, happy

>>Inner quote 'Tis the season for all to be happy. Really happy.
>> happy, happy

code block
~~~~
'this  <a href="http://dontai.com">don</a> is code'
~~~~

Syntax highlighting
```css
#button {
	border: none;
}
```

# Header 1
## Header 2
### Header 3 
#### Header 4 ####
##### Header 5 #####
###### Header 6 ######

Definition Lists
WordPress
:  A semantic personal publishing platform

Abbreviations
Markdown converts text to HTML.
*[HTML]: HyperText Markup Language

Paragraph indentation
&nbsp;&nbsp;&nbsp;&nbsp;I really line a paragraph indent. It really makes the paragraph stand out. I really line a paragraph indent. It really makes the paragraph stand out. I really line a paragraph indent. It really makes the paragraph stand out. I really line a paragraph indent. It really makes the paragraph stand out.

table
|         |          Grouping           |  |

| First Header | Second Header | Third Header |
| ------------ | :-----------: | -----------: |
| Content      |          *Long Cell*        ||
| Content      |   **Cell**    |         Cell |

| New section  |     More      |         Data |
| And more     |            And more         ||


| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |
