---
title: Hello World
---

The more we code the more we realize that sometimes, simple is much better than complex. KISS. KISS Again. KISS, KISS Again. KISS3 Again. KISS4 Again. KISS5 Again. KISS6 Again. Kiss7 Again, KISS8 again, KISS9 again.

It was not long ago that I'd code everything in raw html. In fact, I still enjoy it. On my [main blog Dontai.com](http://dontai.com/wp) even though I use Wordpress as the container, all of my text in each post is html. It seems like old habits die hard.

While I will always enjoy coding, not everyone does. I use Wordpress as a CMS because I reedit many documents and then republish them on a daily basis. I also have the technical chops to monitor and man bots that troll Wordpress sites.

The outside environment is different today. Running 26% of all worldwide sites, Wordpress has a target on its back. If you do not maintain your Wordpress updates and ensure your plugins are current, there is a bot out there that wants to break into your site.

Hence the need for a flat file site. Simple, no fuss, bots cannot break it. In fact, if they do you can just simply reload it.

Flat file sites still do not solve the issue of bot scraping your site, but the security implications are significantly less. This is important.

Welcome to [Hexo](https://hexo.io/)! This is your very first post.
Check [documentation](https://hexo.io/docs/) for more info.
If you get any problems when using Hexo, you can find the answer
in [troubleshooting](https://hexo.io/docs/troubleshooting.html) or
you can ask me on [GitHub](https://github.com/hexojs/hexo/issues).
